<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Gedmo\Translatable\Entity\Translation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="app.default.index")
     *
     * @Template()
     */
    public function indexAction(Request $request)
    {
        return [];
    }
}
